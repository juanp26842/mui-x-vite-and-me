import { blue, orange, red } from "@mui/material/colors";
import { alpha, createTheme, getContrastRatio } from "@mui/material/styles";

// A custom theme for this app
const theme = createTheme({
  palette: {
    mode: "dark",
    primary: {
      main: "#556cd6",
    },
    secondary: {
      main: "#19857b",
    },
    black: {
      main: "#000000", // background? kinda
      contrastText: "#ffffff", // foreground (haven't said that in a while)
      light: "#888888", // Lighter shade of main (Not sure where this is used)
      dark: alpha("#000000", 0.8), // Darker shade of  main (Used in button hovers)
    },
    white: {
      main: "#ffffff",
      contrastText: "#000000",
      light: "#888888",
      dark: "#888888"
    },
    test: {
      main: blue[500],
      dark: alpha(blue[500], 0.7)
    },
    testOrange: {
      main: orange[800],
      dark: orange[900],
      light: orange.A100,
      contrastText: getContrastRatio(orange[800], "#fff") > 4.5 ? "#fff" : "#000" // Chooses the more contrasting between black and white
    },
    tonalOffset: {
      light: 0.1,
      dark: 0.9,
    }, // doesn't seem to work
    error: {
      main: red.A700,
    },
  },
});

export default theme;
