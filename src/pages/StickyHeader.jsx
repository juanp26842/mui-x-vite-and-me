import { Menu, ScreenShare } from "@mui/icons-material";
import viteLogo from "/vite.svg";
import { Button, Card, CardMedia, Divider, Drawer, Grow, Modal, Stack, Typography } from "@mui/material";
import { useState } from "react";

export default function StickyHeader() {

  const [openDrawer, setOpenDrawer] = useState(false);
  const [openModal, setopenModal] = useState(false);
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #aaa",
    boxShadow: 24,
    p: 4,
  };

  return (
    <Stack direction="row" flexWrap="wrap" justifyContent="space-between" alignItems="center" className="w-100 bg-secondary" position="sticky" top={0} zIndex={1}>
      <Card className="ms-2" elevation={5} >
        <Menu sx={{ cursor: "pointer"}} onClick={() => setOpenDrawer(true)}/>
      </Card>
      <Card className="p-1" sx={{ cursor: "pointer"}} elevation={5} onClick={() => setopenModal(true)}>
        <ScreenShare />
        <Typography>Open modal</Typography>
      </Card>
      <Card sx={{width: "200px"}}>
        <CardMedia component="img" src={viteLogo} className="logo p-0" alt="Vite logo" loading="lazy" />
      </Card>
      <Card sx={{width: "300px"}}>
        <CardMedia component="img" src={viteLogo} className="logo p-0" alt="Vite logo" loading="lazy" />
      </Card>
      <Card>
        <CardMedia component="img" src={viteLogo} className="logo p-0" alt="Vite logo" loading="lazy" />
      </Card>
      <Drawer
        anchor="left"
        open={openDrawer}
        onClose={() => setOpenDrawer(false)}
        // sx={{backgroundColor: "lightgray"}} // Changes backdrop
      >
        <Stack
          alignItems="center"
          spacing={4}
          sx={{ width: 300, backgroundColor: "lightgray", minHeight: "60%", paddingTop: "10%"}}

        >
          <Card sx={{width: "50px"}} onClick={() => setOpenDrawer(false)} elevation={5}>
            <CardMedia component="img" src={viteLogo} className="logo p-0" alt="Vite logo" loading="lazy" />
          </Card>
          <Card sx={{width: "50px"}} onClick={() => setOpenDrawer(false)} elevation={5}>
            <CardMedia component="img" src={viteLogo} className="logo p-0" alt="Vite logo" loading="lazy" />
          </Card>
          <Divider sx={{ backgroundColor: "red" }} flexItem component="div" />
          <Card sx={{width: "50px"}} onClick={() => setOpenDrawer(false)} elevation={5}>
            <CardMedia component="img" src={viteLogo} className="logo p-0" alt="Vite logo" loading="lazy" />
          </Card>
        </Stack>
      </Drawer>
      <Modal
        open={openModal}
        onClose={() => setopenModal(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Grow
          in={openModal}
          timeout={500}
        >
          {/* eslint-disable indent */}
          {/* se chingo el eslint xdd por saltar de linea en un comentario */}
          <Stack style={style}> {/* xdd this worked (Without the div with the stack styles the position gets wanky),
            apparently it doesn't work with slider transitions sadge :( */}
            <Stack alignItems="center" sx={style} spacing={3}>
              <Typography id="modal-modal-title" variant="h6" component="h2">
                Text in a modal
              </Typography>
              <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
              </Typography>
              <Button
                variant="contained"
                color="testOrange"
                onClick={() => setopenModal(false)}
                // className="btn btn-dark" // Not sure how to add hover through MUI create theme, but bootstrap is god
                // sx={{"&:hover": {
                //   backgroundColor: "#666666", // Your hover color
                // }}}
                >
                Close
              </Button>
            </Stack>
          {/* eslint-enable indent */}
          </Stack>
        </Grow>
      </Modal>
    </Stack>);
}
