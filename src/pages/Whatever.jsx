import { Typography } from "@mui/material";

export default function Whatever(props) {
  return (
    <>
      <Typography component="h1" {...props}>This is a test</Typography>
    </>
  );
}
