import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import Whatever from "./pages/Whatever";
import { Avatar, Box, Button, Card, CardMedia, Chip, CircularProgress, Divider, FormControlLabel, IconButton, Link, Pagination, Paper, Skeleton, Slider, SpeedDial, SpeedDialAction, SpeedDialIcon, Stack, Switch, Tooltip, Typography, Zoom } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import StickyHeader from "./pages/StickyHeader";
import { AddAlert, ArrowUpwardRounded, CloseOutlined, FaceOutlined, HomeOutlined, Person3Outlined, RemoveRedEye, SaveAsOutlined, ShareOutlined } from "@mui/icons-material";

function App() {
  const [count, setCount] = useState(0);
  const a = "test";
  /* eslint-disable camelcase */
  let Camel_Case = "a";
  const minusZero = 0;
  // /* eslint-enable camelcase */
  console.log(a);
  console.log(1 / -0);
  console.log(1 / +0);
  console.log(Camel_Case);
  /* eslint-disable no-compare-neg-zero */ // Viene por defecto (supongo en el recommended)
  console.log(minusZero === -0);
  Camel_Case = "Change";
  return (
    <Stack maxWidth="100%" className="w-100" alignItems="center" spacing={6.5}>
      <Typography align="center" sx={{ color: "black" }} fontSize="3.7rem" component="h1" variant="h1" className="w-50 text-warning bg-info">Thingy on the top</Typography>
      <Stack direction="row">
        <Link href="https://vitejs.dev" target="_blank" rel="noreferrer" className="my-2">
          <Card>
            <CardMedia component="img" src={viteLogo} className="logo p-0" alt="Vite logo" loading="lazy" />
          </Card>
        </Link>
        <Link href="https://react.dev" target="_blank" rel="noreferrer">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </Link>
      </Stack>
      <StickyHeader/>
      <Typography fontSize="2.5rem">Vite + React</Typography>
      <Stack component={Paper} elevation={20} padding={2} alignItems="center" spacing="5px" justifyContent="space-between">
        <Button variant="contained" onClick={() => setCount((count) => count + 1)} className="w-50 my-3">
          count is {count}
        </Button>
        <Button variant="outlined" onClick={() => setCount((count) => count + 1)} className="w-50">
          count is {count * 2}
        </Button>
        <Button variant="contained" disableElevation onClick={() => setCount((count) => count + 1)} className="w-50">
          count is {count * 3}
        </Button>
        <Typography component="p">
          Edit <Typography component="code">src/App.jsx</Typography> and save to test HMR
        </Typography>
      </Stack>
      <Whatever style={{ fontSize: "2rem" }} className="m-5 text-success" />
      <Typography component="p" variant="h6" className="read-the-docs text-primary ">
        Click on the Vite and React logos to learn more
      </Typography>
      <Divider sx={{ backgroundColor: "whitesmoke", alignSelf: "center" }} light flexItem component="div" variant="middle" ></Divider>
      {/* For variant="text", adjust the height via font-size */}
      <Skeleton sx={{ bgcolor: "red", fontSize: "2rem" }} variant="text">
        <Typography component="span" className="px-2" color="pink" fontSize="2rem" style={{ visibility: "visible" }}>No se usa</Typography>
      </Skeleton>
      {/* For other variants, adjust the size with `width` and `height` */}
      <Divider sx={{ backgroundColor: "green" }} light flexItem component="div" />
      <Skeleton sx={{ bgcolor: "grey.900" }} className="my-2" variant="circular" width={40} height={40} />
      <Divider sx={{ backgroundColor: "blue" }} variant="middle" light flexItem component="div" />
      <Skeleton sx={{ bgcolor: "lightblue" }} className="my-2" variant="rectangular" width={300} height={60} />
      <Divider sx={{ backgroundColor: "red" }} light flexItem component="div" />
      <Skeleton sx={{ bgcolor: "lightblue" }} animation="wave" className="my-2" variant="rounded" width={210} height={60} />
      <Box display="flex" gap={5}>
        {/* Child elements */}
        <Stack justifyContent="center" style={{ backgroundColor: "red", width: "300px", height: "125px" }}>C in box 1</Stack>
        <Stack justifyContent="center" style={{ backgroundColor: "blue", width: "300px", height: "125px" }}>C in box 2</Stack>
        <Stack justifyContent="center" style={{ backgroundColor: "green", width: "300px", height: "125px" }}>C in box 3</Stack>
      </Box>
      <Pagination count={20} variant="text" defaultPage={8} color="black" siblingCount={3} boundaryCount={0} showFirstButton showLastButton className="bg-secondary "></Pagination>
      <Slider
        value={[25, 75]} // Value can be set to more than one state that should be managed with an onChange prop
        defaultValue={50}
        valueLabelDisplay="on"
        step={10}
        marks // Marks on the track
        min={0}
        max={100}
        className="w-50"
        disableSwap // Useful for multiple values
        color="white" // Theme color
        track={false}
      />
      <Stack direction="row" className="bg-warning w-75" justifyContent="space-evenly" alignItems="baseline">
        <Switch
          defaultChecked
          color="primary"
        />
        <Switch
          defaultChecked
          color="white"
          size="small"
        />
        <FormControlLabel control={<Switch
          defaultChecked
          color="black"
        />}
        label="Label"
        color="black"
        className="text-dark" // But color prop doesn't work xdd
        labelPlacement="bottom"
        />
      </Stack>
      <Stack border="Highlight" direction="row" className="w-75" justifyContent="space-evenly" alignItems="baseline">
        <Tooltip TransitionComponent={Zoom} disableInteractive arrow title="Delete" placement="left">
          <IconButton>
            <DeleteIcon color="primary"/>
          </IconButton>
        </Tooltip>
        <Tooltip TransitionComponent={Zoom} disableInteractive arrow title="Profile">
          <IconButton>
            <FaceOutlined color="warning"/>
          </IconButton>
        </Tooltip>
        <Tooltip TransitionComponent={Zoom} disableInteractive arrow title="Show" placement="right">
          <IconButton>
            <RemoveRedEye color="white"/>
          </IconButton>
        </Tooltip>
        <Tooltip TransitionComponent={Zoom} disableInteractive arrow title="Home" placement="right">
          <IconButton>
            <HomeOutlined color="white"/>
          </IconButton>
        </Tooltip>
        <Tooltip title="You don't have permission to do this" followCursor placement="right" enterDelay={200} leaveDelay={100} TransitionComponent={Zoom}>
          <Stack>
            <Button variant="contained" disabled>A Disabled Button</Button>
          </Stack>
        </Tooltip>
        <Tooltip TransitionComponent={Zoom} disableInteractive arrow title="To the top" placement="right">
          <IconButton>
            <Chip
              label="To the top"
              avatar={<Avatar className="text-primary">T</Avatar>} // Overrides icon
              deleteIcon={<ArrowUpwardRounded/>}
              onDelete={() => {}} // OnDelete required in order to show the deleteIcon (Different than onClick which is just the body)
              component="a"
              href="#root"
              color="error"
              variant="outlined"
            />
          </IconButton>
        </Tooltip>
        <CircularProgress color="white"/>
      </Stack>
      <SpeedDial
        ariaLabel="shortcuts" // required
        sx={{ position: "fixed", bottom: 16, right: 16 }}
        icon={<SpeedDialIcon icon={<AddAlert/>} openIcon={<CloseOutlined />} />}
        direction="up"
      >
        <SpeedDialAction
          icon={<ShareOutlined/>}
          tooltipTitle="Share"
          arrow
          disableInteractive
          color="white" // Color doesn't work anywhere
          tooltipOpen // Kinda meh
        />
        <SpeedDialAction
          icon={<SaveAsOutlined/>}
          tooltipTitle="Save"
          arrow
          disableInteractive
          color="white" // Color doesn't work anywhere
        />
        <SpeedDialAction
          icon={<Person3Outlined/>}
          tooltipTitle="Profile"
          arrow
          disableInteractive
          color="danger"
        />
        <SpeedDialAction
          icon={<HomeOutlined/>}
          tooltipTitle="Home"
          arrow
          disableInteractive
          color="black"
        />
      </SpeedDial>
    </Stack>
  );
}

export default App;
